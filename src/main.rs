mod common;

#[tokio::main]
async fn main() {
    common::console::banner();
    common::exec::takeoff(common::conf::load()).await;
}
