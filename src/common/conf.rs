use super::console::fatal;
use clap::Parser;

pub const VERSION: &str = "1.2";

pub struct Params {
    pub statcodes: Vec<u16>,
    pub exclude: bool,
    pub ip: bool,
    pub titles: bool,
    pub favicon: bool,
    pub timestamps: bool,
    pub bodysize: usize
}

#[derive(Parser, Default)]
#[clap(
    author = "tommy touchdown",
    about = "speedboat - lightweight web service aggregator",
    version = VERSION
)]
pub struct Config {
    #[clap(short, long)]
    /// list of target domains and/or ip addresses
    pub list: String,

    #[clap(default_value_t = 100, short, long)]
    /// concurrent workers
    pub threads: usize,

    #[clap(long = "ua")]
    /// custom user agent to operate with
    pub useragent: Option<String>,

    #[clap(long = "mc")]
    /// status codes to match, comma separated
    pub matchcodes: Option<String>,

    #[clap(long = "ec")]
    /// status codes to exclude, comma separated
    pub excludecodes: Option<String>,

    #[clap(long = "ip")]
    /// include ip address
    pub ip: bool,

    #[clap(long = "title")]
    /// retrieve http titles
    pub pulltitles: bool,

    #[clap(long = "redirects")]
    /// follow redirects
    pub follow: bool,

    #[clap(default_value_t = 0, long = "body")]
    /// read n bytes of the response document body
    pub bodysize: usize,

    #[clap(long = "favicon")]
    /// computes an mmh3 favicon hash 
    pub favicon: bool,

    #[clap(long = "ts")]
    /// include timestamps of requests
    pub timestamps: bool,
}

pub fn load() -> Config {
    Config::parse()
}

fn parsecodes(raw: String) -> Vec<u16> {
    let mut codes: Vec<u16> = vec![];
    for code in raw.split(",") {
        let scode: u16 = code
            .parse()
            .unwrap_or_else(|_| fatal("invalid status code provided"));
        codes.push(scode);
    }
    codes
}

pub fn setparams(c: &Config) -> Params {
    let mut statcodes: Vec<u16> = vec![];
    let mut exclude = false;

    if let Some(mcodes) = c.matchcodes.clone() {
        statcodes = parsecodes(mcodes);
    } else if let Some(exclcodes) = c.excludecodes.clone() {
        statcodes = parsecodes(exclcodes);
        exclude = true;
    }

    Params { statcodes, exclude, ip: c.ip, titles: c.pulltitles, favicon: c.favicon,
        timestamps: c.timestamps, bodysize: c.bodysize }
}
