use colored::{ColoredString, Colorize};
use chrono::Local;
use std::process;

use super::conf::VERSION;

pub fn fatal(msg: &str) -> ! {
    println!("{}: {msg}", "fatal".red().bold());
    process::exit(-1);
}

pub fn trunc(s: String, lim: usize) -> String {
    let truncatedstr = s.char_indices()
            .take_while(|(i, _)| *i < lim)
            .map(|(_, c)| c)
            .collect();

    truncatedstr
}

// strips whitespace while maintaining legibility
pub fn fmtwhitespace(s: String) -> String {
    let mut out = String::with_capacity(s.len());
    s.split_whitespace().for_each(|w| {
        if !out.is_empty() {
            out.push(' ');
        }
        out.push_str(w);
    });

    out
}

pub fn fmtcode(code: u16) -> ColoredString {
    match code {
        200..=299 => code.to_string().green().bold(),
        300..=399 => code.to_string().yellow(),
        400..=499 => code.to_string().bright_red(),
        500..=599 => code.to_string().red().bold(),
        _ => code.to_string().bright_black(),
    }
}

pub fn tstamp() -> String {
    let date = Local::now();

    let datestr = format!("{}", date.format("[%Y-%m-%d][%H:%M:%S]"));
    format!("{}", datestr.bright_blue())
}

pub fn parsetitle(s: String) -> String {
    let title: String = trunc(fmtwhitespace(s), 1024);

    format!("{}{}{}",
    "title[".bright_black().bold(),
    title.bright_cyan().bold(),
    "]".bright_black().bold()
    )
}

pub fn parsebody(s: String) -> String {
    format!("{}{}{}",
    "body[".bright_black().bold(),
    s.bright_magenta().bold(),
    "]".bright_black().bold()
    )
}

pub fn parsehash(h: i32) -> String {
    format!("{}{}{}",
    "favicon[".bright_black().bold(),
    h.to_string().blue().bold(),
    "]".bright_black().bold()
    )
}

pub fn parseip(ip: String) -> String {
    format!("{}{}{}",
    "ip[".bright_black().bold(),
    ip.bright_white().bold(),
    "]".bright_black().bold()
    )
}

pub fn parsestatus(sc: u16, url: &String) -> String {
    format!(
        "{} {} {}",
        fmtcode(sc),
        "|".bright_black().bold(),
        url.white().underline(),
    )
}

pub fn banner() {
    eprintln!(r#"
  {}{}    |\___..--"/
        __..--`""       /
'-._.'._:._'-._____..--' {}
"#,         "speed".bright_cyan().bold(),
"boat".bright_magenta().bold(),
VERSION.bright_black());
}
