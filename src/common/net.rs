use reqwest::{redirect::Policy, Client};
use std::time::Duration;

use super::console::{parsestatus, parseip, tstamp};
use super::conf::Params;
use super::modules::*;

pub fn mkclient(redir: bool, ua: String) -> Result<Client, reqwest::Error> {
    let rpolicy: Policy = if redir {
        Policy::limited(3)
    } else {
        Policy::none()
    };

    Client::builder()
        .user_agent(ua)
        .redirect(rpolicy)
        .timeout(Duration::from_secs(2))
        .connect_timeout(Duration::from_millis(500))
        .build()
}

async fn sendreq(c: &Client, use_https: bool, url: &str) -> Result<reqwest::Response, reqwest::Error> {
    let dest: String = if use_https { format!("https://{url}/") } else { format!("http://{url}/") };
    c.get(dest).send().await
}

pub async fn query(
    c: &Client,
    url: &str,
    params: &Params
) -> Result<(), reqwest::Error> {
    let response: reqwest::Response;

    match sendreq(c, true, url).await {
        Ok(res) => response = res,
        Err(e) => {
            if e.is_request() {
                response = sendreq(c, false, url).await?;
            } else {
                return Err(e);
            }
        }
    }

    let ip_addr = response.remote_addr()
        .map(|addr| addr.ip().to_string())
        .unwrap_or("unknown".to_string());

    let sc = response.status().as_u16();

    if !params.statcodes.is_empty() {
        if params.statcodes.contains(&sc) {
            if params.exclude {
                return Ok(());
            }
        } else if !params.exclude {
            return Ok(());
        }
    }

    let url: String = response.url().to_string();
    
    // 10mb response body limit
    let body_raw = readnbody(response, 10485760).await?;
    let body = body_str(body_raw).unwrap_or("error parsing response body".to_string());

    let mut out = parsestatus(sc, &url);

    if params.ip {
        out = format!("{} {}", out, parseip(ip_addr));
    }

    if params.timestamps {
        out = format!("{} {}", tstamp(), out);
    }

    if params.titles {
        out = format!("{} {}", out, get_title(&body));
    }

    if params.favicon {
        let hash = hash_favicon(&body, url).await;
        if !hash.is_empty() {
            out = format!("{} {}", out, hash);
        }
    }

    if params.bodysize > 0 {
        out = format!("{} {}", out, body_contents(&body, params.bodysize));
    }

    println!("{}", out);

    Ok(())
}
