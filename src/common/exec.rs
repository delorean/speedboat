use futures::{stream, StreamExt};
use std::{
    fs::File,
    io::{BufRead, BufReader},
};

use super::{
    conf::{Config, setparams},
    console::fatal,
    net::{mkclient, query},
};

pub async fn takeoff(args: Config) {
    let params = setparams(&args);
    
    let mut ua: String = "Mozilla/5.0 AppleWebKit/537.36 (KHTML, like Gecko; compatible; Googlebot/2.1; +http://www.google.com/bot.html) Chrome/126.0.0.0 Safari/537.36".to_string();
    if let Some(custom_ua) = args.useragent {
        ua = custom_ua;
    }

    let client = mkclient(args.follow, ua).unwrap_or_else(|_| fatal("error instantiating http client"));

    let file = File::open(&args.list)
        .unwrap_or_else(|e| fatal(format!("unable to read file: {e}").as_str()));

    let buf = BufReader::new(file);

    stream::iter(buf.lines())
        .for_each_concurrent(args.threads, |line| {
            // workers using the same client ref > each worker getting their own? we'll see 
            let wc = &client;
            let qparams = &params;
            async move {
                let _ = query(
                    wc,
                    line.unwrap_or_else(|_| fatal("error attempting buffered read")).trim(),
                    qparams
                )
                .await;
            }
        })
        .await;
}
