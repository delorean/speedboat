use select::{document::Document, predicate::Name};
use reqwest::{Response, Url};
use base64::{Engine as _, engine::general_purpose};
use bytes::{Bytes, BytesMut};
use murmur3::murmur3_32;
use std::{error::Error, io::Cursor};

use super::console::{parsetitle, parsebody, parsehash, fmtwhitespace, trunc};

pub fn get_title(body: &str) -> String {
    let document = Document::from(body);

    let title = document
    .find(Name("title"))
    .next()
    .map(|n| n.text())
    .unwrap_or_else(|| "".to_string());
    
    parsetitle(title)
}

pub async fn readnbody(mut resp: Response, n: usize) -> Result<Bytes, reqwest::Error> {
    let mut body = BytesMut::new();
    while let Some(chunk) = resp.chunk().await? {
        if !(body.len() + chunk.len()) > n {
            body.extend_from_slice(&chunk); 
        }   
    }

    Ok(body.freeze())
}

pub fn body_str(body: Bytes) -> Result<String, Box<dyn Error>> {
    String::from_utf8(body.to_vec())
        .map_err(|e| format!("error reading body into a string: {}", e).into())
}

pub fn body_contents(body: &str, lim: usize) -> String {
    let document = Document::from(body);

    let mut bodytext = document.find(Name("body")).next().map(|n| n.text()).unwrap_or_else(|| "".to_string());

    bodytext = fmtwhitespace(bodytext);
    if bodytext.len() > lim {
        bodytext = trunc(bodytext, lim);
    }

    parsebody(bodytext)
}

fn fmtbase64(s: &str, interval: usize, sep: char) -> String {
    let mut out = String::with_capacity(s.len() + s.len() / interval);
    let mut count = 0;

    for (_, c) in s.chars().enumerate() {
        out.push(c);
        count += 1;
        if count == interval {
            out.push(sep);
            count = 0;
        }
    }
    if count != 0 {
        out.push(sep);
    }

    out
}

fn faviconurl(doc: Document, url: String ) -> Result<String, Box <dyn Error>> {
    for node in doc.find(Name("link")) {
        if let Some(rel) = node.attr("rel") {
            if rel.eq("icon") {
                if let Some(href) = node.attr("href") {
                    let base_url = Url::parse(&url)?;
                    let favicon_url = base_url.join(href)?;
                    return Ok(favicon_url.to_string());
                }
            }
        }
    }

    Err("".into())
}

async fn dl_favicon(url: String) -> Result<Vec<u8>, Box<dyn Error>> {
    let data = reqwest::get(url).await?.bytes().await?.to_vec();
    Ok(data)
}

pub async fn hash_favicon(body: &str, url: String) -> String {
    let document = Document::from(body);

    if let Ok(favurl) = faviconurl(document, url) {
        if let Ok(data) = dl_favicon(favurl).await {
            // compute hash
            let b64 = general_purpose::STANDARD.encode(data);
            let f_b64 = fmtbase64(&b64, 76,'\n');
            let hash = murmur3_32(&mut Cursor::new(f_b64.into_bytes()), 0).unwrap_or_else(|_| 0) as i32;
            return parsehash(hash);
        }
    }

    "".into()
}
